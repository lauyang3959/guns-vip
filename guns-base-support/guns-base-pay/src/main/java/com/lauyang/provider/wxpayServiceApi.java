package com.lauyang.provider;
import cn.stylefeng.roses.kernel.model.response.ResponseData;
import com.lauyang.base.BaesRequest;
import com.lauyang.base.payPram;

import java.util.Map;

/**
 * 微信支付接口
 */
public interface wxpayServiceApi {
    /**
     * 微信支付接口
     *
     */
    public ResponseData doUnifiedOrder(payPram baseRequest);
}
