package com.lauyang.provider;

import cn.stylefeng.roses.kernel.model.response.ResponseData;
import com.github.wxpay.sdk.MyConfig;
import com.github.wxpay.sdk.WXPay;
import com.github.wxpay.sdk.WXPayUtil;
import com.lauyang.base.BaesRequest;
import com.lauyang.base.consts;
import com.lauyang.base.payPram;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

/**
 * 微信支付接口
 */
public class wxpayServiceProvider implements wxpayServiceApi{

    /**
     * 微信统一下单接口
     * @return
     */
//    @RequestMapping(value = "/doUnifiedOrder", method = RequestMethod.POST)
    public ResponseData doUnifiedOrder(payPram baseRequest) {
        ResponseData response = new ResponseData();
        Map resultMap=new HashMap();
        //String openid = baseRequest.getRequestData().getOpenId();


        MyConfig config = null;
        WXPay wxpay =null;
        try {
            config = new MyConfig();
            wxpay= new WXPay(config);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //生成的随机字符串
        String nonce_str = WXPayUtil.generateNonceStr();
        //获取客户端的ip地址
        //todo 动态参数 注释
        //获取本机的ip地址
        InetAddress addr = null;
        try {
            addr = InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        String spbill_create_ip = addr.getHostAddress();
        //支付金额，需要转成字符串类型，否则后面的签名会失败
        int  total_fee=1;
        //商品描述
        String body = "测试支付";
        //商户订单号
        String out_trade_no= WXPayUtil.generateNonceStr();


        //统一下单接口参数
        HashMap<String, String> data = new HashMap<String, String>();
        data.put("appid", consts.APPID);
        data.put("mch_id", consts.WX_MCH_ID);
        data.put("nonce_str", nonce_str);
        data.put("body", body);
        data.put("out_trade_no",out_trade_no);
        data.put("total_fee", String.valueOf(total_fee));
        data.put("spbill_create_ip", spbill_create_ip);
        data.put("notify_url", consts.WX_NOTIFY_URL);
        //交易类型
        data.put("trade_type",consts.WX_trade_type);
        data.put("openid", openid);

        try {
            Map<String, String> rMap = wxpay.unifiedOrder(data);
            System.out.println("统一下单接口返回: " + rMap);
            String return_code = (String) rMap.get("return_code");
            String result_code = (String) rMap.get("result_code");
            String nonceStr = WXPayUtil.generateNonceStr();
            resultMap.put("nonceStr", nonceStr);
            Long timeStamp = System.currentTimeMillis() / 1000;
            if ("SUCCESS".equals(return_code) && return_code.equals(result_code)) {
                String prepayid = rMap.get("prepay_id");
                resultMap.put("package", "prepay_id="+prepayid);
                resultMap.put("signType", "MD5");
                //这边要将返回的时间戳转化成字符串，不然小程序端调用wx.requestPayment方法会报签名错误
                resultMap.put("timeStamp", timeStamp + "");
                //再次签名，这个签名用于小程序端调用wx.requesetPayment方法
                resultMap.put("appId",consts.APPID);
                String sign = WXPayUtil.generateSignature(resultMap, key);
                resultMap.put("paySign", sign);
                System.out.println("生成的签名paySign : "+ sign);
                response.setData(resultMap);
                return response;
            }else{
                return  response;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return  response;
        }
    }
}
