package com.lauyang.base;

public class BaesRequest<T> {
    /**
     *  请求对象
     */
    T requestData;


    public BaesRequest(){
        super();
    }

    public T getRequestData(){
        return requestData;
    }

    public void setRequestData(T requestData) {
        this.requestData = requestData;
    }

}
