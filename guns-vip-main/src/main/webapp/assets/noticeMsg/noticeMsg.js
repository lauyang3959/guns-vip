layui.use(['table', 'admin', 'ax', 'func'], function () {
    var $ = layui.$;
    var table = layui.table;
    var $ax = layui.ax;
    var admin = layui.admin;
    var func = layui.func;

    /**
     * 通知消息管理
     */
    var NoticeMsg = {
        tableId: "noticeMsgTable"
    };

    /**
     * 初始化表格的列
     */
    NoticeMsg.initColumn = function () {
        return [[
            {type: 'checkbox'},
            {field: 'id', hide: true, title: 'id'},
            {field: 'userId', sort: true, title: '用户id'},
            {field: 'msg', sort: true, title: '消息内容'},
            {align: 'center', toolbar: '#tableBar', title: '操作'}
        ]];
    };

    /**
     * 点击查询按钮
     */
    NoticeMsg.search = function () {
        var queryData = {};
        queryData['condition'] = $("#condition").val();
        table.reload(NoticeMsg.tableId, {
            where: queryData, page: {curr: 1}
        });
    };

    /**
     * 弹出添加对话框
     */
    NoticeMsg.openAddDlg = function () {
        func.open({
            title: '添加通知消息',
            content: Feng.ctxPath + '/noticeMsg/add',
            tableId: NoticeMsg.tableId
        });
    };

    /**
    * 点击编辑
    *
    * @param data 点击按钮时候的行数据
    */
    NoticeMsg.openEditDlg = function (data) {
        func.open({
            title: '修改通知消息',
            content: Feng.ctxPath + '/noticeMsg/edit?id=' + data.id,
            tableId: NoticeMsg.tableId
        });
    };

    /**
     * 导出excel按钮
     */
    NoticeMsg.exportExcel = function () {
        var checkRows = table.checkStatus(NoticeMsg.tableId);
        if (checkRows.data.length === 0) {
            Feng.error("请选择要导出的数据");
        } else {
            table.exportFile(tableResult.config.id, checkRows.data, 'xls');
        }
    };

    /**
     * 点击删除
     *
     * @param data 点击按钮时候的行数据
     */
    NoticeMsg.onDeleteItem = function (data) {
        var operation = function () {
            var ajax = new $ax(Feng.ctxPath + "/noticeMsg/delete", function (data) {
                Feng.success("删除成功!");
                table.reload(NoticeMsg.tableId);
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("id", data.id);
            ajax.start();
        };
        Feng.confirm("是否删除?", operation);
    };

    // 渲染表格
    var tableResult = table.render({
        elem: '#' + NoticeMsg.tableId,
        url: Feng.ctxPath + '/noticeMsg/list',
        page: true,
        height: "full-158",
        cellMinWidth: 100,
        cols: NoticeMsg.initColumn()
    });

    // 搜索按钮点击事件
    $('#btnSearch').click(function () {
        NoticeMsg.search();
    });

    // 添加按钮点击事件
    $('#btnAdd').click(function () {
        NoticeMsg.openAddDlg();
    });

    // 导出excel
    $('#btnExp').click(function () {
        NoticeMsg.exportExcel();
    });

    // 工具条点击事件
    table.on('tool(' + NoticeMsg.tableId + ')', function (obj) {
        var data = obj.data;
        var layEvent = obj.event;

        if (layEvent === 'edit') {
            NoticeMsg.openEditDlg(data);
        } else if (layEvent === 'delete') {
            NoticeMsg.onDeleteItem(data);
        }
    });
});
