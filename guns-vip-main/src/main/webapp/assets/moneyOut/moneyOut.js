layui.use(['table', 'admin', 'ax', 'func'], function () {
    var $ = layui.$;
    var table = layui.table;
    var $ax = layui.ax;
    var admin = layui.admin;
    var func = layui.func;

    /**
     * 提现管理
     */
    var MoneyOut = {
        tableId: "moneyOutTable"
    };

    /**
     * 初始化表格的列
     */
    MoneyOut.initColumn = function () {
        return [[
            {type: 'checkbox'},
            {field: 'id', hide: true, title: ''},
            {field: 'userId', sort: true, title: '用户编号'},
            {field: 'userName', sort: true, title: '用户昵称'},
            {field: 'userTel', sort: true, title: '用户联系方式'},
            {field: 'bankUserName', sort: true, title: '开户人姓名'},
            {field: 'bankName', sort: true, title: '开户行名称'},
            {field: 'bankCard', sort: true, title: '银行卡号'},
            {field: 'bankAddr', sort: true, title: '开户行地址'},
            {field: 'state', sort: true, title: '状态'},
            {field: 'creatTime', sort: true, title: '创建时间'},
            {align: 'center', toolbar: '#tableBar', title: '操作'}
        ]];
    };

    /**
     * 点击查询按钮
     */
    MoneyOut.search = function () {
        var queryData = {};
        queryData['condition'] = $("#condition").val();
        table.reload(MoneyOut.tableId, {
            where: queryData, page: {curr: 1}
        });
    };

    /**
     * 弹出添加对话框
     */
    MoneyOut.openAddDlg = function () {
        func.open({
            title: '添加提现',
            content: Feng.ctxPath + '/moneyOut/add',
            tableId: MoneyOut.tableId
        });
    };

    /**
    * 点击编辑
    *
    * @param data 点击按钮时候的行数据
    */
    MoneyOut.openEditDlg = function (data) {
        func.open({
            title: '修改提现',
            content: Feng.ctxPath + '/moneyOut/edit?id=' + data.id,
            tableId: MoneyOut.tableId
        });
    };

    /**
     * 导出excel按钮
     */
    MoneyOut.exportExcel = function () {
        var checkRows = table.checkStatus(MoneyOut.tableId);
        if (checkRows.data.length === 0) {
            Feng.error("请选择要导出的数据");
        } else {
            table.exportFile(tableResult.config.id, checkRows.data, 'xls');
        }
    };

    /**
     * 点击删除
     *
     * @param data 点击按钮时候的行数据
     */
    MoneyOut.onDeleteItem = function (data) {
        var operation = function () {
            var ajax = new $ax(Feng.ctxPath + "/moneyOut/delete", function (data) {
                Feng.success("删除成功!");
                table.reload(MoneyOut.tableId);
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("id", data.id);
            ajax.start();
        };
        Feng.confirm("是否删除?", operation);
    };

    // 渲染表格
    var tableResult = table.render({
        elem: '#' + MoneyOut.tableId,
        url: Feng.ctxPath + '/moneyOut/list',
        page: true,
        height: "full-158",
        cellMinWidth: 100,
        cols: MoneyOut.initColumn()
    });

    // 搜索按钮点击事件
    $('#btnSearch').click(function () {
        MoneyOut.search();
    });

    // 添加按钮点击事件
    $('#btnAdd').click(function () {
        MoneyOut.openAddDlg();
    });

    // 导出excel
    $('#btnExp').click(function () {
        MoneyOut.exportExcel();
    });

    // 工具条点击事件
    table.on('tool(' + MoneyOut.tableId + ')', function (obj) {
        var data = obj.data;
        var layEvent = obj.event;

        if (layEvent === 'edit') {
            MoneyOut.openEditDlg(data);
        } else if (layEvent === 'delete') {
            MoneyOut.onDeleteItem(data);
        }
    });
});
