layui.use(['table', 'admin', 'ax', 'func'], function () {
    var $ = layui.$;
    var table = layui.table;
    var $ax = layui.ax;
    var admin = layui.admin;
    var func = layui.func;

    /**
     * 支付单管理
     */
    var PayOrder = {
        tableId: "payOrderTable"
    };

    /**
     * 初始化表格的列
     */
    PayOrder.initColumn = function () {
        return [[
            {type: 'checkbox'},
            {field: 'id', hide: true, title: ''},
            {field: 'orderNumber', sort: true, title: '订单编号'},
            {field: 'userName', sort: true, title: '用户昵称'},
            {field: 'payMoney', sort: true, title: '操作金额'},
            {field: 'payType', sort: true, title: '支付类型'},
            {field: 'payTime', sort: true, title: '支付时间'},
            {align: 'center', toolbar: '#tableBar', title: '操作'}
        ]];
    };

    /**
     * 点击查询按钮
     */
    PayOrder.search = function () {
        var queryData = {};
        queryData['condition'] = $("#condition").val();
        table.reload(PayOrder.tableId, {
            where: queryData, page: {curr: 1}
        });
    };

    /**
     * 弹出添加对话框
     */
    PayOrder.openAddDlg = function () {
        func.open({
            title: '添加支付单',
            content: Feng.ctxPath + '/payOrder/add',
            tableId: PayOrder.tableId
        });
    };

    /**
    * 点击编辑
    *
    * @param data 点击按钮时候的行数据
    */
    PayOrder.openEditDlg = function (data) {
        func.open({
            title: '修改支付单',
            content: Feng.ctxPath + '/payOrder/edit?id=' + data.id,
            tableId: PayOrder.tableId
        });
    };

    /**
     * 导出excel按钮
     */
    PayOrder.exportExcel = function () {
        var checkRows = table.checkStatus(PayOrder.tableId);
        if (checkRows.data.length === 0) {
            Feng.error("请选择要导出的数据");
        } else {
            table.exportFile(tableResult.config.id, checkRows.data, 'xls');
        }
    };

    /**
     * 点击删除
     *
     * @param data 点击按钮时候的行数据
     */
    PayOrder.onDeleteItem = function (data) {
        var operation = function () {
            var ajax = new $ax(Feng.ctxPath + "/payOrder/delete", function (data) {
                Feng.success("删除成功!");
                table.reload(PayOrder.tableId);
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("id", data.id);
            ajax.start();
        };
        Feng.confirm("是否删除?", operation);
    };

    // 渲染表格
    var tableResult = table.render({
        elem: '#' + PayOrder.tableId,
        url: Feng.ctxPath + '/payOrder/list',
        page: true,
        height: "full-158",
        cellMinWidth: 100,
        cols: PayOrder.initColumn()
    });

    // 搜索按钮点击事件
    $('#btnSearch').click(function () {
        PayOrder.search();
    });

    // 添加按钮点击事件
    $('#btnAdd').click(function () {
        PayOrder.openAddDlg();
    });

    // 导出excel
    $('#btnExp').click(function () {
        PayOrder.exportExcel();
    });

    // 工具条点击事件
    table.on('tool(' + PayOrder.tableId + ')', function (obj) {
        var data = obj.data;
        var layEvent = obj.event;

        if (layEvent === 'edit') {
            PayOrder.openEditDlg(data);
        } else if (layEvent === 'delete') {
            PayOrder.onDeleteItem(data);
        }
    });
});
