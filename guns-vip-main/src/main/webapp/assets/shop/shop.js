layui.use(['table', 'admin', 'ax', 'func'], function () {
    var $ = layui.$;
    var table = layui.table;
    var $ax = layui.ax;
    var admin = layui.admin;
    var func = layui.func;

    /**
     * 店铺管理
     */
    var Shop = {
        tableId: "shopTable"
    };

    /**
     * 初始化表格的列
     */
    Shop.initColumn = function () {
        return [[
            {type: 'checkbox'},
            {field: 'id', hide: true, title: ''},
            {field: 'shopName', sort: true, title: '店铺名称'},
            {field: 'shopTel', sort: true, title: '店铺联系方式'},
            {field: 'shopAddr', sort: true, title: '店铺地址'},
            {field: 'shopTime', sort: true, title: '店铺营业时间'},
            {field: 'creatTime', sort: true, title: '创建时间'},
            {align: 'center', toolbar: '#tableBar', title: '操作'}
        ]];
    };

    /**
     * 点击查询按钮
     */
    Shop.search = function () {
        var queryData = {};
        queryData['condition'] = $("#condition").val();
        table.reload(Shop.tableId, {
            where: queryData, page: {curr: 1}
        });
    };

    /**
     * 弹出添加对话框
     */
    Shop.openAddDlg = function () {
        func.open({
            title: '添加店铺',
            content: Feng.ctxPath + '/shop/add',
            tableId: Shop.tableId
        });
    };

    /**
    * 点击编辑
    *
    * @param data 点击按钮时候的行数据
    */
    Shop.openEditDlg = function (data) {
        func.open({
            title: '修改店铺',
            content: Feng.ctxPath + '/shop/edit?id=' + data.id,
            tableId: Shop.tableId
        });
    };

    /**
     * 导出excel按钮
     */
    Shop.exportExcel = function () {
        var checkRows = table.checkStatus(Shop.tableId);
        if (checkRows.data.length === 0) {
            Feng.error("请选择要导出的数据");
        } else {
            table.exportFile(tableResult.config.id, checkRows.data, 'xls');
        }
    };

    /**
     * 点击删除
     *
     * @param data 点击按钮时候的行数据
     */
    Shop.onDeleteItem = function (data) {
        var operation = function () {
            var ajax = new $ax(Feng.ctxPath + "/shop/delete", function (data) {
                Feng.success("删除成功!");
                table.reload(Shop.tableId);
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("id", data.id);
            ajax.start();
        };
        Feng.confirm("是否删除?", operation);
    };

    // 渲染表格
    var tableResult = table.render({
        elem: '#' + Shop.tableId,
        url: Feng.ctxPath + '/shop/list',
        page: true,
        height: "full-158",
        cellMinWidth: 100,
        cols: Shop.initColumn()
    });

    // 搜索按钮点击事件
    $('#btnSearch').click(function () {
        Shop.search();
    });

    // 添加按钮点击事件
    $('#btnAdd').click(function () {
        Shop.openAddDlg();
    });

    // 导出excel
    $('#btnExp').click(function () {
        Shop.exportExcel();
    });

    // 工具条点击事件
    table.on('tool(' + Shop.tableId + ')', function (obj) {
        var data = obj.data;
        var layEvent = obj.event;

        if (layEvent === 'edit') {
            Shop.openEditDlg(data);
        } else if (layEvent === 'delete') {
            Shop.onDeleteItem(data);
        }
    });
});
