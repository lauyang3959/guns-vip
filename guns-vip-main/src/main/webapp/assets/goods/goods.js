layui.use(['table', 'admin', 'ax', 'func'], function () {
    var $ = layui.$;
    var table = layui.table;
    var $ax = layui.ax;
    var admin = layui.admin;
    var func = layui.func;

    /**
     * 商品管理管理
     */
    var Goods = {
        tableId: "goodsTable"
    };

    /**
     * 初始化表格的列
     */
    Goods.initColumn = function () {
        return [[
            {type: 'checkbox'},
            {field: 'id', hide: true, title: '主键'},
            {field: 'goodsName', sort: true, title: '商品名称'},
            {field: 'goodsPrice', sort: true, title: '商品价格'},
            {field: 'goodsSaleNum', sort: true, title: '销量'},
            {field: 'shopName', sort: true, title: '商铺名'},
            {field: 'shopTel', sort: true, title: '商铺电话'},
            {field: 'shopAddr', sort: true, title: '商铺地址'},
            {field: 'goodsAttribute', sort: true, title: '商品属性'},
            {field: 'coverImgUrl', sort: true, title: '封面图'},
            {field: 'disFlag', sort: true, title: '是否禁用'},
            {field: 'indexFlag', sort: true, title: '是否推荐到首页Y N'},
            {field: 'goodsIndexImgUrl', sort: true, title: '首页轮播图'},
            {field: 'goodsBannerImgUrl', sort: true, title: '商品轮播图'},
            {field: 'createTime', sort: true, title: '创建时间'},
            {field: 'createUser', sort: true, title: '创建人'},
            {field: 'updateTime', sort: true, title: '更新时间'},
            {field: 'updateUser', sort: true, title: '更新人'},
            {align: 'center', toolbar: '#tableBar', title: '操作'}
        ]];
    };

    /**
     * 点击查询按钮
     */
    Goods.search = function () {
        var queryData = {};
        queryData['condition'] = $("#condition").val();
        table.reload(Goods.tableId, {
            where: queryData, page: {curr: 1}
        });
    };

    /**
     * 弹出添加对话框
     */
    Goods.openAddDlg = function () {
        func.open({
            title: '添加商品管理',
            content: Feng.ctxPath + '/goods/add',
            tableId: Goods.tableId
        });
    };

    /**
    * 点击编辑
    *
    * @param data 点击按钮时候的行数据
    */
    Goods.openEditDlg = function (data) {
        func.open({
            title: '修改商品管理',
            content: Feng.ctxPath + '/goods/edit?id=' + data.id,
            tableId: Goods.tableId
        });
    };

    /**
     * 导出excel按钮
     */
    Goods.exportExcel = function () {
        var checkRows = table.checkStatus(Goods.tableId);
        if (checkRows.data.length === 0) {
            Feng.error("请选择要导出的数据");
        } else {
            table.exportFile(tableResult.config.id, checkRows.data, 'xls');
        }
    };

    /**
     * 点击删除
     *
     * @param data 点击按钮时候的行数据
     */
    Goods.onDeleteItem = function (data) {
        var operation = function () {
            var ajax = new $ax(Feng.ctxPath + "/goods/delete", function (data) {
                Feng.success("删除成功!");
                table.reload(Goods.tableId);
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("id", data.id);
            ajax.start();
        };
        Feng.confirm("是否删除?", operation);
    };

    // 渲染表格
    var tableResult = table.render({
        elem: '#' + Goods.tableId,
        url: Feng.ctxPath + '/goods/list',
        page: true,
        height: "full-158",
        cellMinWidth: 100,
        cols: Goods.initColumn()
    });

    // 搜索按钮点击事件
    $('#btnSearch').click(function () {
        Goods.search();
    });

    // 添加按钮点击事件
    $('#btnAdd').click(function () {
        Goods.openAddDlg();
    });

    // 导出excel
    $('#btnExp').click(function () {
        Goods.exportExcel();
    });

    // 工具条点击事件
    table.on('tool(' + Goods.tableId + ')', function (obj) {
        var data = obj.data;
        var layEvent = obj.event;

        if (layEvent === 'edit') {
            Goods.openEditDlg(data);
        } else if (layEvent === 'delete') {
            Goods.onDeleteItem(data);
        }
    });
});
