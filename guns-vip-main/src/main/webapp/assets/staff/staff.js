layui.use(['table', 'admin', 'ax', 'func'], function () {
    var $ = layui.$;
    var table = layui.table;
    var $ax = layui.ax;
    var admin = layui.admin;
    var func = layui.func;

    /**
     * 用户信息管理
     */
    var Staff = {
        tableId: "staffTable"
    };

    /**
     * 初始化表格的列
     */
    Staff.initColumn = function () {
        return [[
            {type: 'checkbox'},
            {field: 'id', hide: true, title: ''},
            {field: 'avatarUrl', sort: true, title: '用户头像'},
            {field: 'nickName', sort: true, title: '用户昵称'},
            {field: 'money', sort: true, title: '余额'},
            {field: 'commission', sort: true, title: '佣金'},
            {field: 'memberLevel', sort: true, title: '会员等级'},
            {field: 'wechatNumber', sort: true, title: '微信号'},
            {field: 'tel', sort: true, title: '电话号码'},
            {field: 'bankUserName', sort: true, title: '开户人姓名'},
            {field: 'bankName', sort: true, title: '开户行名称'},
            {field: 'bankCard', sort: true, title: '银行卡号'},
            {field: 'bankAddr', sort: true, title: '开户行地址'},
            {field: 'creatTime', sort: true, title: '创建时间'},
            {align: 'center', toolbar: '#tableBar', title: '操作'}
        ]];
    };

    /**
     * 点击查询按钮
     */
    Staff.search = function () {
        var queryData = {};
        queryData['condition'] = $("#condition").val();
        table.reload(Staff.tableId, {
            where: queryData, page: {curr: 1}
        });
    };

    /**
     * 弹出添加对话框
     */
    Staff.openAddDlg = function () {
        func.open({
            title: '添加用户信息',
            content: Feng.ctxPath + '/staff/add',
            tableId: Staff.tableId
        });
    };

    /**
    * 点击编辑
    *
    * @param data 点击按钮时候的行数据
    */
    Staff.openEditDlg = function (data) {
        func.open({
            title: '修改用户信息',
            content: Feng.ctxPath + '/staff/edit?id=' + data.id,
            tableId: Staff.tableId
        });
    };

    /**
     * 导出excel按钮
     */
    Staff.exportExcel = function () {
        var checkRows = table.checkStatus(Staff.tableId);
        if (checkRows.data.length === 0) {
            Feng.error("请选择要导出的数据");
        } else {
            table.exportFile(tableResult.config.id, checkRows.data, 'xls');
        }
    };

    /**
     * 点击删除
     *
     * @param data 点击按钮时候的行数据
     */
    Staff.onDeleteItem = function (data) {
        var operation = function () {
            var ajax = new $ax(Feng.ctxPath + "/staff/delete", function (data) {
                Feng.success("删除成功!");
                table.reload(Staff.tableId);
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("id", data.id);
            ajax.start();
        };
        Feng.confirm("是否删除?", operation);
    };

    // 渲染表格
    var tableResult = table.render({
        elem: '#' + Staff.tableId,
        url: Feng.ctxPath + '/staff/list',
        page: true,
        height: "full-158",
        cellMinWidth: 100,
        cols: Staff.initColumn()
    });

    // 搜索按钮点击事件
    $('#btnSearch').click(function () {
        Staff.search();
    });

    // 添加按钮点击事件
    $('#btnAdd').click(function () {
        Staff.openAddDlg();
    });

    // 导出excel
    $('#btnExp').click(function () {
        Staff.exportExcel();
    });

    // 工具条点击事件
    table.on('tool(' + Staff.tableId + ')', function (obj) {
        var data = obj.data;
        var layEvent = obj.event;

        if (layEvent === 'edit') {
            Staff.openEditDlg(data);
        } else if (layEvent === 'delete') {
            Staff.onDeleteItem(data);
        }
    });
});
