layui.use(['table', 'admin', 'ax', 'func'], function () {
    var $ = layui.$;
    var table = layui.table;
    var $ax = layui.ax;
    var admin = layui.admin;
    var func = layui.func;

    /**
     * 资金日志管理
     */
    var MoneyLog = {
        tableId: "moneyLogTable"
    };

    /**
     * 初始化表格的列
     */
    MoneyLog.initColumn = function () {
        return [[
            {type: 'checkbox'},
            {field: 'id', hide: true, title: ''},
            {field: 'userName', sort: true, title: '操作人'},
            {field: 'type', sort: true, title: '类型'},
            {field: 'money', sort: true, title: '操作金额'},
            {field: 'balance', sort: true, title: '余额'},
            {field: 'describe', sort: true, title: '描述'},
            {field: 'creatTime', sort: true, title: '操作时间'},
            {align: 'center', toolbar: '#tableBar', title: '操作'}
        ]];
    };

    /**
     * 点击查询按钮
     */
    MoneyLog.search = function () {
        var queryData = {};
        queryData['condition'] = $("#condition").val();
        table.reload(MoneyLog.tableId, {
            where: queryData, page: {curr: 1}
        });
    };

    /**
     * 弹出添加对话框
     */
    MoneyLog.openAddDlg = function () {
        func.open({
            title: '添加资金日志',
            content: Feng.ctxPath + '/moneyLog/add',
            tableId: MoneyLog.tableId
        });
    };

    /**
    * 点击编辑
    *
    * @param data 点击按钮时候的行数据
    */
    MoneyLog.openEditDlg = function (data) {
        func.open({
            title: '修改资金日志',
            content: Feng.ctxPath + '/moneyLog/edit?id=' + data.id,
            tableId: MoneyLog.tableId
        });
    };

    /**
     * 导出excel按钮
     */
    MoneyLog.exportExcel = function () {
        var checkRows = table.checkStatus(MoneyLog.tableId);
        if (checkRows.data.length === 0) {
            Feng.error("请选择要导出的数据");
        } else {
            table.exportFile(tableResult.config.id, checkRows.data, 'xls');
        }
    };

    /**
     * 点击删除
     *
     * @param data 点击按钮时候的行数据
     */
    MoneyLog.onDeleteItem = function (data) {
        var operation = function () {
            var ajax = new $ax(Feng.ctxPath + "/moneyLog/delete", function (data) {
                Feng.success("删除成功!");
                table.reload(MoneyLog.tableId);
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("id", data.id);
            ajax.start();
        };
        Feng.confirm("是否删除?", operation);
    };

    // 渲染表格
    var tableResult = table.render({
        elem: '#' + MoneyLog.tableId,
        url: Feng.ctxPath + '/moneyLog/list',
        page: true,
        height: "full-158",
        cellMinWidth: 100,
        cols: MoneyLog.initColumn()
    });

    // 搜索按钮点击事件
    $('#btnSearch').click(function () {
        MoneyLog.search();
    });

    // 添加按钮点击事件
    $('#btnAdd').click(function () {
        MoneyLog.openAddDlg();
    });

    // 导出excel
    $('#btnExp').click(function () {
        MoneyLog.exportExcel();
    });

    // 工具条点击事件
    table.on('tool(' + MoneyLog.tableId + ')', function (obj) {
        var data = obj.data;
        var layEvent = obj.event;

        if (layEvent === 'edit') {
            MoneyLog.openEditDlg(data);
        } else if (layEvent === 'delete') {
            MoneyLog.onDeleteItem(data);
        }
    });
});
