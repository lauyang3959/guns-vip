layui.use(['table', 'admin', 'ax', 'func'], function () {
    var $ = layui.$;
    var table = layui.table;
    var $ax = layui.ax;
    var admin = layui.admin;
    var func = layui.func;

    /**
     * 用户信息管理
     */
    var UserInfo = {
        tableId: "userInfoTable"
    };

    /**
     * 初始化表格的列
     */
    UserInfo.initColumn = function () {
        return [[
            {type: 'checkbox'},
            {field: 'id', hide: true, title: ''},
            {field: 'avatarUrl', sort: true, title: '用户头像'},
            {field: 'nickName', sort: true, title: '用户昵称'},
            {field: 'money', sort: true, title: '余额'},
            {field: 'commission', sort: true, title: '佣金'},
            {field: 'superiorId', sort: true, title: '上级id'},
            {field: 'userType', sort: true, title: '用户类型'},
            {field: 'memberLevel', sort: true, title: '会员等级'},
            {field: 'wechatNumber', sort: true, title: '微信号'},
            {field: 'tel', sort: true, title: '电话号码'},
            {field: 'bankUserName', sort: true, title: '开户人姓名'},
            {field: 'bankName', sort: true, title: '开户行名称'},
            {field: 'bankCard', sort: true, title: '银行卡号'},
            {field: 'bankAddr', sort: true, title: '开户行地址'},
            {field: 'creatTime', sort: true, title: '创建时间'},
            {align: 'center', toolbar: '#tableBar', title: '操作'}
        ]];
    };

    /**
     * 点击查询按钮
     */
    UserInfo.search = function () {
        var queryData = {};
        queryData['condition'] = $("#condition").val();
        table.reload(UserInfo.tableId, {
            where: queryData, page: {curr: 1}
        });
    };

    /**
     * 弹出添加对话框
     */
    UserInfo.openAddDlg = function () {
        func.open({
            title: '添加用户信息',
            content: Feng.ctxPath + '/userInfo/add',
            tableId: UserInfo.tableId
        });
    };

    /**
    * 点击编辑
    *
    * @param data 点击按钮时候的行数据
    */
    UserInfo.openEditDlg = function (data) {
        func.open({
            title: '修改用户信息',
            content: Feng.ctxPath + '/userInfo/edit?id=' + data.id,
            tableId: UserInfo.tableId
        });
    };

    /**
     * 导出excel按钮
     */
    UserInfo.exportExcel = function () {
        var checkRows = table.checkStatus(UserInfo.tableId);
        if (checkRows.data.length === 0) {
            Feng.error("请选择要导出的数据");
        } else {
            table.exportFile(tableResult.config.id, checkRows.data, 'xls');
        }
    };

    /**
     * 点击删除
     *
     * @param data 点击按钮时候的行数据
     */
    UserInfo.onDeleteItem = function (data) {
        var operation = function () {
            var ajax = new $ax(Feng.ctxPath + "/userInfo/delete", function (data) {
                Feng.success("删除成功!");
                table.reload(UserInfo.tableId);
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("id", data.id);
            ajax.start();
        };
        Feng.confirm("是否删除?", operation);
    };

    // 渲染表格
    var tableResult = table.render({
        elem: '#' + UserInfo.tableId,
        url: Feng.ctxPath + '/userInfo/list',
        page: true,
        height: "full-158",
        cellMinWidth: 100,
        cols: UserInfo.initColumn()
    });

    // 搜索按钮点击事件
    $('#btnSearch').click(function () {
        UserInfo.search();
    });

    // 添加按钮点击事件
    $('#btnAdd').click(function () {
        UserInfo.openAddDlg();
    });

    // 导出excel
    $('#btnExp').click(function () {
        UserInfo.exportExcel();
    });

    // 工具条点击事件
    table.on('tool(' + UserInfo.tableId + ')', function (obj) {
        var data = obj.data;
        var layEvent = obj.event;

        if (layEvent === 'edit') {
            UserInfo.openEditDlg(data);
        } else if (layEvent === 'delete') {
            UserInfo.onDeleteItem(data);
        }
    });
});
