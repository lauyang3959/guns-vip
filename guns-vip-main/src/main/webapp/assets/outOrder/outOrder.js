layui.use(['table', 'admin', 'ax', 'func'], function () {
    var $ = layui.$;
    var table = layui.table;
    var $ax = layui.ax;
    var admin = layui.admin;
    var func = layui.func;

    /**
     * 退款单管理
     */
    var OutOrder = {
        tableId: "outOrderTable"
    };

    /**
     * 初始化表格的列
     */
    OutOrder.initColumn = function () {
        return [[
            {type: 'checkbox'},
            {field: 'id', hide: true, title: ''},
            {field: 'orderNumber', sort: true, title: '订单编号'},
            {field: 'userName', sort: true, title: '用户昵称'},
            {field: 'outMoney', sort: true, title: '退款金额'},
            {field: 'outWay', sort: true, title: '退款方式'},
            {field: 'outType', sort: true, title: '类型'},
            {field: 'state', sort: true, title: '退款状态'},
            {field: 'creatTime', sort: true, title: '申请时间'},
            {align: 'center', toolbar: '#tableBar', title: '操作'}
        ]];
    };

    /**
     * 点击查询按钮
     */
    OutOrder.search = function () {
        var queryData = {};
        queryData['condition'] = $("#condition").val();
        table.reload(OutOrder.tableId, {
            where: queryData, page: {curr: 1}
        });
    };

    /**
     * 弹出添加对话框
     */
    OutOrder.openAddDlg = function () {
        func.open({
            title: '添加退款单',
            content: Feng.ctxPath + '/outOrder/add',
            tableId: OutOrder.tableId
        });
    };

    /**
    * 点击编辑
    *
    * @param data 点击按钮时候的行数据
    */
    OutOrder.openEditDlg = function (data) {
        func.open({
            title: '修改退款单',
            content: Feng.ctxPath + '/outOrder/edit?id=' + data.id,
            tableId: OutOrder.tableId
        });
    };

    /**
     * 导出excel按钮
     */
    OutOrder.exportExcel = function () {
        var checkRows = table.checkStatus(OutOrder.tableId);
        if (checkRows.data.length === 0) {
            Feng.error("请选择要导出的数据");
        } else {
            table.exportFile(tableResult.config.id, checkRows.data, 'xls');
        }
    };

    /**
     * 点击删除
     *
     * @param data 点击按钮时候的行数据
     */
    OutOrder.onDeleteItem = function (data) {
        var operation = function () {
            var ajax = new $ax(Feng.ctxPath + "/outOrder/delete", function (data) {
                Feng.success("删除成功!");
                table.reload(OutOrder.tableId);
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("id", data.id);
            ajax.start();
        };
        Feng.confirm("是否删除?", operation);
    };

    // 渲染表格
    var tableResult = table.render({
        elem: '#' + OutOrder.tableId,
        url: Feng.ctxPath + '/outOrder/list',
        page: true,
        height: "full-158",
        cellMinWidth: 100,
        cols: OutOrder.initColumn()
    });

    // 搜索按钮点击事件
    $('#btnSearch').click(function () {
        OutOrder.search();
    });

    // 添加按钮点击事件
    $('#btnAdd').click(function () {
        OutOrder.openAddDlg();
    });

    // 导出excel
    $('#btnExp').click(function () {
        OutOrder.exportExcel();
    });

    // 工具条点击事件
    table.on('tool(' + OutOrder.tableId + ')', function (obj) {
        var data = obj.data;
        var layEvent = obj.event;

        if (layEvent === 'edit') {
            OutOrder.openEditDlg(data);
        } else if (layEvent === 'delete') {
            OutOrder.onDeleteItem(data);
        }
    });
});
