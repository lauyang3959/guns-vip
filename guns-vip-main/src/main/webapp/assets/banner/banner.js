layui.use(['table', 'admin', 'ax', 'func'], function () {
    var $ = layui.$;
    var table = layui.table;
    var $ax = layui.ax;
    var admin = layui.admin;
    var func = layui.func;

    /**
     * banner管理
     */
    var Banner = {
        tableId: "bannerTable"
    };

    /**
     * 初始化表格的列
     */
    Banner.initColumn = function () {
        return [[
            {type: 'checkbox'},
            {field: 'id', hide: true, title: ''},
            {field: 'bannerImgUrl', sort: true, title: 'banner图'},
            {field: 'bannerType', sort: true, title: '是否跳转到商品'},
            {field: 'goodsId', sort: true, title: '商品id'},
            {field: 'bannerState', sort: true, title: '禁用状态'},
            {field: 'sort', sort: true, title: '排序(越小越靠前)'},
            {field: 'creatTime', sort: true, title: '创建时间'},
            {align: 'center', toolbar: '#tableBar', title: '操作'}
        ]];
    };

    /**
     * 点击查询按钮
     */
    Banner.search = function () {
        var queryData = {};
        queryData['condition'] = $("#condition").val();
        table.reload(Banner.tableId, {
            where: queryData, page: {curr: 1}
        });
    };

    /**
     * 弹出添加对话框
     */
    Banner.openAddDlg = function () {
        func.open({
            title: '添加banner',
            content: Feng.ctxPath + '/banner/add',
            tableId: Banner.tableId
        });
    };

    /**
    * 点击编辑
    *
    * @param data 点击按钮时候的行数据
    */
    Banner.openEditDlg = function (data) {
        func.open({
            title: '修改banner',
            content: Feng.ctxPath + '/banner/edit?id=' + data.id,
            tableId: Banner.tableId
        });
    };

    /**
     * 导出excel按钮
     */
    Banner.exportExcel = function () {
        var checkRows = table.checkStatus(Banner.tableId);
        if (checkRows.data.length === 0) {
            Feng.error("请选择要导出的数据");
        } else {
            table.exportFile(tableResult.config.id, checkRows.data, 'xls');
        }
    };

    /**
     * 点击删除
     *
     * @param data 点击按钮时候的行数据
     */
    Banner.onDeleteItem = function (data) {
        var operation = function () {
            var ajax = new $ax(Feng.ctxPath + "/banner/delete", function (data) {
                Feng.success("删除成功!");
                table.reload(Banner.tableId);
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("id", data.id);
            ajax.start();
        };
        Feng.confirm("是否删除?", operation);
    };

    // 渲染表格
    var tableResult = table.render({
        elem: '#' + Banner.tableId,
        url: Feng.ctxPath + '/banner/list',
        page: true,
        height: "full-158",
        cellMinWidth: 100,
        cols: Banner.initColumn()
    });

    // 搜索按钮点击事件
    $('#btnSearch').click(function () {
        Banner.search();
    });

    // 添加按钮点击事件
    $('#btnAdd').click(function () {
        Banner.openAddDlg();
    });

    // 导出excel
    $('#btnExp').click(function () {
        Banner.exportExcel();
    });

    // 工具条点击事件
    table.on('tool(' + Banner.tableId + ')', function (obj) {
        var data = obj.data;
        var layEvent = obj.event;

        if (layEvent === 'edit') {
            Banner.openEditDlg(data);
        } else if (layEvent === 'delete') {
            Banner.onDeleteItem(data);
        }
    });
});
