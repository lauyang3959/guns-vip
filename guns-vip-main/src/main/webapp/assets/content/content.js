layui.use(['table', 'admin', 'ax', 'func'], function () {
    var $ = layui.$;
    var table = layui.table;
    var $ax = layui.ax;
    var admin = layui.admin;
    var func = layui.func;

    /**
     * 管理
     */
    var Content = {
        tableId: "contentTable"
    };

    /**
     * 初始化表格的列
     */
    Content.initColumn = function () {
        return [[
            {type: 'checkbox'},
            {field: 'id', hide: true, title: ''},
            {field: 'aboutUs', sort: true, title: '关于我们'},
            {field: 'contactTel', sort: true, title: '客服电话'},
            {align: 'center', toolbar: '#tableBar', title: '操作'}
        ]];
    };

    /**
     * 点击查询按钮
     */
    Content.search = function () {
        var queryData = {};
        queryData['condition'] = $("#condition").val();
        table.reload(Content.tableId, {
            where: queryData, page: {curr: 1}
        });
    };

    /**
     * 弹出添加对话框
     */
    Content.openAddDlg = function () {
        func.open({
            title: '添加',
            content: Feng.ctxPath + '/content/add',
            tableId: Content.tableId
        });
    };

    /**
    * 点击编辑
    *
    * @param data 点击按钮时候的行数据
    */
    Content.openEditDlg = function (data) {
        func.open({
            title: '修改',
            content: Feng.ctxPath + '/content/edit?id=' + data.id,
            tableId: Content.tableId
        });
    };

    /**
     * 导出excel按钮
     */
    Content.exportExcel = function () {
        var checkRows = table.checkStatus(Content.tableId);
        if (checkRows.data.length === 0) {
            Feng.error("请选择要导出的数据");
        } else {
            table.exportFile(tableResult.config.id, checkRows.data, 'xls');
        }
    };

    /**
     * 点击删除
     *
     * @param data 点击按钮时候的行数据
     */
    Content.onDeleteItem = function (data) {
        var operation = function () {
            var ajax = new $ax(Feng.ctxPath + "/content/delete", function (data) {
                Feng.success("删除成功!");
                table.reload(Content.tableId);
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("id", data.id);
            ajax.start();
        };
        Feng.confirm("是否删除?", operation);
    };

    // 渲染表格
    var tableResult = table.render({
        elem: '#' + Content.tableId,
        url: Feng.ctxPath + '/content/list',
        page: true,
        height: "full-158",
        cellMinWidth: 100,
        cols: Content.initColumn()
    });

    // 搜索按钮点击事件
    $('#btnSearch').click(function () {
        Content.search();
    });

    // 添加按钮点击事件
    $('#btnAdd').click(function () {
        Content.openAddDlg();
    });

    // 导出excel
    $('#btnExp').click(function () {
        Content.exportExcel();
    });

    // 工具条点击事件
    table.on('tool(' + Content.tableId + ')', function (obj) {
        var data = obj.data;
        var layEvent = obj.event;

        if (layEvent === 'edit') {
            Content.openEditDlg(data);
        } else if (layEvent === 'delete') {
            Content.onDeleteItem(data);
        }
    });
});
